#!/usr/bin/env bash

gem install compass

npm install -g yo --save-dev
npm install -g generator-angular --save-dev
