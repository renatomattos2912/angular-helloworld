(function() {
	'use strict';
	
	angular.module('app.cadastro.srvc', [])
	.factory('CadastroSrvc', ['$firebase', 'FB_URL', 
		function($firebase, FB_URL){			
			
			var refCadastros = new Firebase(FB_URL + 'cadastros');
			
			var cadastros = $firebase(refCadastros);						
			
			var Cadastro = {
				all: cadastros,
				add: function(cad){	
					return cadastros.$add(cad);
				},
				findById: function(id){
					return cadastros.$child(id);
				},
				update: function(id, cad){					
					return cadastros.$child(id).$set(cad);
				},
				del: function(id){
					return cadastros.$remove(id);
				}
			};
			
			return Cadastro;
		}
	]);	
	
}).call(this);

