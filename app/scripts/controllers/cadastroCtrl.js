(function() {
	'use strict';
	
	angular.module('app.cadastro.ctrl', [])
	.controller('CadastroCtrl', ['$scope', '$routeParams', '$location', 'CadastroSrvc',
		function($scope, $routeParams, $location, CadastroSrvc){					
			$scope.cadastros = CadastroSrvc.all;			
			
			$scope.add = function(){				
				CadastroSrvc.add($scope.cadastro);
			};
			
			$scope.update = function() {
				CadastroSrvc.update($routeParams.id, $scope.resultado);
			}
			
			$scope.del = function(){
				CadastroSrvc.del($routeParams.id);
			}
			
			if($location.path() === '/cadastro/'+$routeParams.id){
				$scope.resultado = CadastroSrvc.findById($routeParams.id);
				
			}
			
		}
	]);	
	
}).call(this);

