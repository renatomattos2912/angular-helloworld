(function() {
    'use strict';
	angular.module('app', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch',
		'mgcrea.ngStrap',
		'app.cadastro.srvc',
		'app.cadastro.ctrl',		
		'firebase'
	  ])
	  .config(['$routeProvider',
		  function($routeProvider) {
			$routeProvider		  
			  .when('/cadastro', {
				templateUrl: 'views/cadastro.html'        
			  })
			  .when('/cadastro/:id', {
				templateUrl: 'views/resultado.html'        
			  })
			  .otherwise({
				redirectTo: '/cadastro'
			  });
	   }
	])
	.constant('FB_URL', 'https://angular-helloworld.firebaseio.com/');
	
}).call(this);
